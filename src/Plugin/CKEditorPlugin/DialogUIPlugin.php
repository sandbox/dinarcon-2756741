<?php

namespace Drupal\ckeditor_dialogui\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "dialogui" plugin.
 *
 * @CKEditorPlugin(
 *   id = "dialogui",
 *   label = @Translation("Dialog User Interface Plugin"),
 * )
 */
class DialogUIPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return base_path() . 'libraries/dialogui/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

}
