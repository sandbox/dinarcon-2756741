CKEditor Dialog User Interface
==============================

Description
===========
This plugin defines various dialog field types to be used in the dialog, which
is mostly used in accompany with the dialog plugin.

Installation
============

This module requires the core CKEditor module.

1. Download the plugin from http://ckeditor.com/addon/dialogui compatible
   with CKEditor versions 4.5 or higher.
2. Place the plugin in the root libraries folder (/libraries).
3. Enable CKEditor User Interface module in the Drupal admin.

Uninstallation
==============
1. Uninstall the module from 'Administer >> Modules'.

MAINTAINERS
===========
Mauricio Dinarte - https://www.drupal.org/u/dinarcon

Credits
=======
Initial development and maintenance by Agaric.
